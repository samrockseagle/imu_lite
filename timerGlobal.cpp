#include "timerGlobal.h"

timerGlobal::timerGlobal()
{
  running = false;
}
timerGlobal::~timerGlobal()
{
  timeStop();
}

void timerGlobal::timeStart()
{
  time = millis();
  running = true;
}

void timerGlobal::timeStop()
{
  if (running) {
    time = 0;
    running = false;
  }
}

unsigned long timerGlobal::timeReturn()
{
  if (running)
  {
    if (millis() > time)
      return (millis() - time);
    else
      return (time - millis());
  }
  else
  {
    return 0;
  }
}


bool timerGlobal::timerfn(unsigned int set)
{
  if (timeReturn() < set)
  {
    return false;
  }
  else
  {
    return true;
  }
}


