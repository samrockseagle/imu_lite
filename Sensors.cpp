//-----------------------------------------------------------------------------
// Wrapper developed by KRDP -AKA sam (samrockseagle@gmail.com)
// Original project by Dennis Frie - 2012 - Dennis.frie@gmail.com
// Discussion: http://www.rcgroups.com/forums/showthread.php?t=1677559
//
// Other contributors to this code:
//  Mark Mansur (Mangus on rcgroups)


#include "Sensors.h"
#include <Wire.h>


void Sensors::dummy()
{
  for (int i =0 ; i < 2; ++i)
  {
    Serial.println("HELLO");
  }
}

// Function used to write to I2C:
void Sensors::i2cWrite(int device, byte address, byte val)
{
  Wire.beginTransmission(device);
  Wire.write(address);
  Wire.write(val);
  Wire.endTransmission();
}

// Function to read from I2C
void Sensors::i2cRead(int device, char address, char bytesToRead)
{
  Wire.beginTransmission(device);
  Wire.write(address);
  Wire.endTransmission();
  Wire.beginTransmission(device);
  Wire.requestFrom(device, bytesToRead);
  char i = 0;
  while (Wire.available())
  {
    sensorBuffer[i++] = Wire.read();
  }
  Wire.endTransmission();
}

void Sensors::tkOut()
{
  htk_data[0] = (byte)(abs(tiltAngleLP - tiltStart + 90) / 2);
  htk_data[1] = (byte)abs(rollAngleLP - rollStart + 90);
  htk_data[2] = (byte)(abs(panAngleLP + 180) / 2);
  htk_data[3] = (byte)255;
 // Serial.write(htk_data, 4);
}
//--------------------------------------------------------------------------------------
// Func: UpdateSensors
// Desc: Retrieves the sensor data from the sensor board via I2C.
//--------------------------------------------------------------------------------------
void Sensors::sensUpdate()
{
  // Read x, y, z acceleration, pack the data.
  i2cRead(ADXL345_ADDR, ADXL345_X_ADDR, 6);
  accRaw[0] = ((int)sensorBuffer[0] | ((int)sensorBuffer[1] << 8)) * -1;
  accRaw[1] = ((int)sensorBuffer[2] | ((int)sensorBuffer[3] << 8)) * -1;
  accRaw[2] = (int)sensorBuffer[4] | ((int)sensorBuffer[5] << 8);


  // Read x, y, z from gyro, pack the data
  i2cRead(ITG3205_ADDR, ITG3205_X_ADDR, 6);
  gyroRaw[0] = (int)sensorBuffer[1] | ((int)sensorBuffer[0] << 8);
  gyroRaw[1] = ((int)sensorBuffer[3] | ((int)sensorBuffer[2] << 8)) * -1;
  gyroRaw[2] = ((int)sensorBuffer[5] | ((int)sensorBuffer[4] << 8)) * -1;


  // Read x, y, z from magnetometer;
  i2cRead(HMC_ADDR, HMC_X_ADDR, 6);
  for (unsigned char i = 0; i < 3; i++)
  {
    magRaw[i] = (int)sensorBuffer[(i * 2) + 1] | ((int)sensorBuffer[i * 2] << 8);
  }
}

//--------------------------------------------------------------------------------------
// Func: setGyro_offset
// Desc: Sets the gyro offset.
//--------------------------------------------------------------------------------------
void Sensors::setGyro_offset()
{
  // Not sure what the outer loops are for, Dennis. Stabilization time?
  //
  for (unsigned int i = 0; i < 100; i++)
  {
    sensUpdate();
    for (unsigned char k = 0; k < 3; k++)
    {
      gyroOff[k] += gyroRaw[k];
    }
  }

  for (unsigned char k = 0; k < 3; k++)
  {
    gyroOff[k] = gyroOff[k] / 100;
  }
}

//--------------------------------------------------------------------------------------
// Func: calibMag
// Desc:
//--------------------------------------------------------------------------------------
void Sensors::calibMag()
{
  i2cWrite(HMC_ADDR, 0x00, 0b00010001);

  // MM: Again with the loops. Not sure what purpose this serves, Dennis.
  for (unsigned char i = 0; i < 40; i++)
  {
    sensUpdate();
  }
  magPosOff[0] = magRaw[0];
  magPosOff[1] = magRaw[1];
  magPosOff[2] = magRaw[2];


  i2cWrite(HMC_ADDR, 0x00, 0b00010010);

  for (unsigned char i = 0; i < 40; i++)
  {
    sensUpdate();
  }
  magNegOff[0] = magRaw[0];
  magNegOff[1] = magRaw[1];
  magNegOff[2] = magRaw[2];

  i2cWrite(HMC_ADDR, 0x00, 0b00010000);

  magGain[0] = -2500 / float(magNegOff[0] - magPosOff[0]);
  magGain[1] = -2500 / float(magNegOff[1] - magPosOff[1]);
  magGain[2] = -2500 / float(magNegOff[2] - magPosOff[2]);

  for (unsigned char i = 0; i < 40; i++)
  {
    sensUpdate();
  }
}


//--------------------------------------------------------------------------------------
// Func: GyroCalc
// Desc: Calculate angle from gyro-data
//--------------------------------------------------------------------------------------
void Sensors::gyroCal()
{
#if (DEBUG)
  for (unsigned char i = 0; i < 3; i++)
  {
    angleRaw[i] += ((gyroRaw[i] - gyroOff[i]));
    angle[i] = angleRaw[i] / (SAMPLERATE * SCALING_FACTOR);
  }
#endif
}

//--------------------------------------------------------------------------------------
// Func: AccelCalc
// Desc: Calculate angle from accelerometer data
//--------------------------------------------------------------------------------------
void Sensors::accelCal()
{
  accRaw[0] += accOffset[0];
  accRaw[1] += accOffset[1];
  accRaw[2] += accOffset[2];

  for (unsigned char i = 0; i < 3; i++)
  {
    accG[i] = (float)accRaw[i] / ACC_SENS;
  }

  // So, lets calculate R
  // R^2 = Rx^2+Ry^2+Rz^2
#if (ASSUME_1G_ACC == 0)
  R = sqrt((accG[0] * accG[0]) + (accG[1] * accG[1]) + (accG[2] * accG[2]));
#else // Otherwise, just assume total G = 1.
  R = 1;
#endif

  // Calculate final angles:
  if (R < 1.3 && R > 0.7)
  {
    for (unsigned char i = 0; i < 3; i++)
    {
      accAngle[i] = acos(accG[i] / R) * 57.3;
    }
  }
}

//--------------------------------------------------------------------------------------
// Func: MagCalc
// Desc: Calculates angle from magnetometer data.
//--------------------------------------------------------------------------------------
void Sensors::magCal()
{
  // Invert 2 axis
  magRaw[1] *= -1;
  magRaw[2] *= -1;

  // Set gain:
  magRaw[0] *= magGain[0];
  magRaw[1] *= magGain[1];
  magRaw[2] *= magGain[2];

  magRaw[0] -= magOffset[0];
  magRaw[1] -= magOffset[1];
  magRaw[2] -= magOffset[2];

  float testAngle = tiltAngle - 90;
  mx = magRaw[0] * cos((testAngle) / 57.3)
       + magRaw[1] * sin(testAngle / 57.3);

  my = magRaw[0] * sin((rollAngle - 90) / 57.3)
       * sin((tiltAngle - 90) / 57.3)
       + magRaw[2] * cos((rollAngle - 90) / 57.3)
       - magRaw[1] * sin((rollAngle - 90) / 57.3)
       * cos((tiltAngle - 90) / 57.3);

  // Calculate pan-angle from magnetometer.
  magAngle[2] = (atan(mx / my) * 57.3 + 90);

  // Get full 0-360 degrees.
  if (my < 0)
  {
    magAngle[2] += 180;
  }

  float tempAngle = panStart - magAngle[2];

  if (tempAngle > 180)
  {
    tempAngle -= 360;
  }
  else if (tempAngle < -180)
  {
    tempAngle += 360;
  }

  magAngle[2] = tempAngle * -1;
}


//--------------------------------------------------------------------------------------
// Func: Filter
// Desc: Filters / merges sensor data.
//--------------------------------------------------------------------------------------
void Sensors::filterData()
{
  // Used to set initial values.
  if (reset)
  {
#if FATSHARK_HT_MODULE
    digitalWrite(BUZZER, HIGH);
#endif
    reset = false;

    tiltStart = 0;
    panStart = 0;
    rollStart = 0;

    sensUpdate();
    gyroCal();
    accelCal();
    magCal();

    panAngle = 0;
    tiltStart = accAngle[0];
    panStart = magAngle[2];
    rollStart = accAngle[1];

#if FATSHARK_HT_MODULE
    digitalWrite(BUZZER, LOW);
#endif
  }

  // Simple filterData, uses mainly gyro-data, but uses accelerometer to compensate for drift
  rollAngle = (rollAngle + ((gyroRaw[0] - gyroOff[0]) * cos((tiltAngle - 90) / 57.3) + (gyroRaw[2] - gyroOff[2]) * sin((tiltAngle - 90) / 57.3)) / (SAMPLERATE * SCALING_FACTOR)) * gyroWeightTiltRoll + accAngle[1] * (1 - gyroWeightTiltRoll);
  tiltAngle = (tiltAngle + ((gyroRaw[1] - gyroOff[1]) * cos((rollAngle - 90) / 57.3) + (gyroRaw[2] - gyroOff[2]) * sin((rollAngle - 90) / 57.3) * -1) / (SAMPLERATE * SCALING_FACTOR)) * gyroWeightTiltRoll + accAngle[0] * (1 - gyroWeightTiltRoll);
  panAngle = (panAngle + ((gyroRaw[2] - gyroOff[2]) * cos((tiltAngle - 90) / 57.3) + (((gyroRaw[0] - gyroOff[0]) * -1) * (sin((tiltAngle - 90) / 57.3))) + (((gyroRaw[1] - gyroOff[1]) * 1) * (sin((rollAngle - 90) / 57.3)))) / (SAMPLERATE * SCALING_FACTOR)) * GyroWeightPan + magAngle[2] * (1 - GyroWeightPan);

  if (TrackerStarted)
  {
    // All low-pass filters
    tiltAngleLP = tiltAngle * tiltRollBeta + (1 - tiltRollBeta) * lastTiltAngle;
    lastTiltAngle = tiltAngleLP;

    rollAngleLP = rollAngle * tiltRollBeta + (1 - tiltRollBeta) * lastRollAngle;
    lastRollAngle = rollAngleLP;

    panAngleLP = panAngle * panBeta + (1 - panBeta) * lastPanAngle;
    lastPanAngle = panAngleLP;
  }
}

//--------------------------------------------------------------------------------------
// Func: initializeSensors
// Desc: Initializes the sensor board sensors.
//--------------------------------------------------------------------------------------
void Sensors::initializeSensors()
{
  i2cRead(ITG3205_ADDR, 0x00, 1);
  ITG3205_ID = sensorBuffer[0];

  i2cRead(ADXL345_ADDR, 0x00, 1);
  ADXL345_ID = sensorBuffer[0];

  // Accelerometer increase G-range (+/- 16G)
  i2cWrite(ADXL345_ADDR, 0x31, 0b00001011);
  i2cRead(HMC_ADDR, 0x00, 1);
  HMC_ID = sensorBuffer[0];

  i2cWrite(ITG3205_ADDR, 22, 24);

  //  ADXL345 POWER_CTL
  i2cWrite(ADXL345_ADDR, 0x2D, 0);
  i2cWrite(ADXL345_ADDR, 0x2D, 16);
  i2cWrite(ADXL345_ADDR, 0x2D, 8);

  // HMC5883
  // Run in continuous mode
  i2cWrite(HMC_ADDR, 0x02, 0x00);

#if (ALWAYS_CAL_GYRO)
  // Set sensor offset
  setGyro_offset();
#endif
}

//--------------------------------------------------------------------------------------
// Func: centerReset
// Desc: Utility for resetting tracker center. This is only called during tracker
//       startup. Button press resets are handled during filtering. (Needs refactor)
//--------------------------------------------------------------------------------------
void Sensors::centerReset()
{
  for (unsigned char k = 0; k < 250; k++)
  {
    sensUpdate();
    gyroCal();
    accelCal();
    magCal();
    filterData();
  }

  tiltStart = accAngle[0];
  panStart = magAngle[2];
  rollStart = accAngle[1];

  sensUpdate();
  gyroCal();
  accelCal();
  magCal();
  filterData();

  panAngle = magAngle[2];
  tiltAngle = accAngle[0];
  rollAngle = accAngle[1];

  TrackerStarted = true;
}

