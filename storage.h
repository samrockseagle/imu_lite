#ifndef _STG_H
#define _STG_H

#include <avr/eeprom.h>
#include <Arduino.h>
#include <EEPROM.h>

class Storage
{
public:
	bool queryifNew();
	void saveSetValues();
	void saveMagConf();
	void saveAccConf();
	void GetData();	
};

#endif
