
#ifndef _CONFIG_H
#define _CONFIG_H

#define DEBUG 0

#define ALWAYS_CAL_GYRO 0

#define BUTTON_INPUT 5
//#define ARDUINO_LED 13

// Output serial data to host evern X frames
#define SERIAL_OUTPUT_FRAME_INTERVAL    2

// Serial communication speed. 
#define SERIAL_BAUD 57600

// Sensor board update-rate. Not done yet. 
#define UPDATE_RATE 50


#define ITG3205_ADDR 0x68    // The address of ITG3205
#define ITG3205_X_ADDR 0x1D  // Start address for x-axis
#define SCALING_FACTOR 13     // Scaling factor - used when converting to angle

// Accelerometer
//

#define ADXL345_ADDR (0x53)  // The adress of ADXL345 
#define ADXL345_X_ADDR (0x32)// Start address for x-axis
#define ACC_SENS 256         // Sensitivity. 13 bit adc, +/- 16 g. Calculated as: (2^13)/(16*2)
#define ASSUME_1G_ACC 0      // Assuming the total gravitation is 1. True if only earth gravitation has influence.  


// Magnetometer
//
#define HMC_ADDR 0x1E        // The address of HMC5883
#define HMC_X_ADDR (0x03)    // Start address for x-axis. 

#define SAMPLERATE 128       // Samplerate of sensors (in hz, samples per second)

#define MAG0MAX 625
#define MAG1MAX 625
#define MAG2MAX 625
#define MAG0MIN -625
#define MAG1MIN -625
#define MAG2MIN -625

#endif
