#include "storage.h"
#include "Sensors.h"

extern Sensors sens;

bool Storage::queryifNew()
{
	if (EEPROM.read(0) != 8)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Storage::saveSetValues()
{
	EEPROM.write(1, (byte)(sens.tiltRollBeta * 100));
	EEPROM.write(2, (byte)(sens.panBeta * 100));
	EEPROM.write(3, (byte)(sens.gyroWeightTiltRoll * 100));
	EEPROM.write(4, (byte)(sens.GyroWeightPan * 100));


	// Saving gyro calibration values
	int temp = (int)(sens.gyroOff[0] + 500.5);
	EEPROM.write(35, (byte)temp);
	EEPROM.write(36, (byte)(temp >> 8));

	temp = (int)(sens.gyroOff[1] + 500.5);
	EEPROM.write(37, (byte)temp);
	EEPROM.write(38, (byte)(temp >> 8));

	temp = (int)(sens.gyroOff[2] + 500.5);
	EEPROM.write(39, (byte)temp);
	EEPROM.write(40, (byte)(temp >> 8));

	EEPROM.write(0, 8);

	Serial.println("Settings saved!");
}


void Storage::saveMagConf()
{
	int temp = (int)(sens.magOffset[0] + 2000);
	EEPROM.write(200, (byte)temp);
	EEPROM.write(201, (byte)(temp >> 8));

	temp = (int)(sens.magOffset[1] + 2000);
	EEPROM.write(202, (byte)temp);
	EEPROM.write(203, (byte)(temp >> 8));

	temp = (int)(sens.magOffset[2] + 2000);
	EEPROM.write(204, (byte)temp);
	EEPROM.write(205, (byte)(temp >> 8));
}

void Storage::saveAccConf()
{
	int temp = (int)(sens.accOffset[0] + 2000);
	EEPROM.write(206, (byte)temp);
	EEPROM.write(207, (byte)(temp >> 8));

	temp = (int)(sens.accOffset[1] + 2000);
	EEPROM.write(208, (byte)temp);
	EEPROM.write(209, (byte)(temp >> 8));

	temp = (int)(sens.accOffset[2] + 2000);
	EEPROM.write(210, (byte)temp);
	EEPROM.write(211, (byte)(temp >> 8));

}


void Storage::GetData()
{
	sens.tiltRollBeta = (float)EEPROM.read(1) / 100;
	sens.panBeta = (float)EEPROM.read(2) / 100;
	sens.gyroWeightTiltRoll = (float)EEPROM.read(3) / 100;
	sens.GyroWeightPan = (float)EEPROM.read(4) / 100;
   
	sens.gyroOff[0] = EEPROM.read(35) + (EEPROM.read(36) << 8) - 500;
	sens.gyroOff[1] = EEPROM.read(37) + (EEPROM.read(38) << 8) - 500;
	sens.gyroOff[2] = EEPROM.read(39) + (EEPROM.read(40) << 8) - 500;

	sens.magOffset[0] = EEPROM.read(200) + (EEPROM.read(201) << 8) - 2000;
	sens.magOffset[1] = EEPROM.read(202) + (EEPROM.read(203) << 8) - 2000;
	sens.magOffset[2] = EEPROM.read(204) + (EEPROM.read(205) << 8) - 2000;

	sens.accOffset[0] = EEPROM.read(206) + (EEPROM.read(207) << 8) - 2000;
	sens.accOffset[1] = EEPROM.read(208) + (EEPROM.read(209) << 8) - 2000;
	sens.accOffset[2] = EEPROM.read(210) + (EEPROM.read(211) << 8) - 2000;

}



