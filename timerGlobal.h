#ifndef TIMERGLOBAL_H
#define TIMERGLOBAL_H

#define TIMEBASE 1000
#define TIMEBASEM (TIMEBASE*60)

#include<Arduino.h>

class timerGlobal
{
public:
	timerGlobal();
	~timerGlobal();
	void timeStart();
	void timeStop();
	unsigned long timeReturn();
	bool timerfn(unsigned int);
	bool running;
private:
	unsigned long time;
};

#endif
