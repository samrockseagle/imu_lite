#ifndef _SENS_H
#define _SENS_H

#include <Arduino.h>
#include "Config.h"

class Sensors
{
public:
  void dummy();
	void setGyro_offset();
	void i2cWrite(int, byte, byte); /*Sensors::i2cWrite(int device, byte address, byte val)*/
	void i2cRead(int , char, char);
	void centerReset();
	void SensorInfoPrint();

	void tkOut();
	void sensUpdate();

	void calibMag();
	void magCal();
	void accelCal();
	void gyroCal();

	void filterData();
	void initializeSensors();

	byte htk_data[4];
	bool reset;

private:
	unsigned char ADXL345_ID = 0;
	unsigned char ITG3205_ID = 0;
	unsigned char HMC_ID = 0;

	byte sensorBuffer[10];       // Buffer for bytes read from sensors

	long accRaw[3];              // Raw readings from accelerometer
	float accG[3];               // G-force in each direction
	float accAngle[3];           // Measured angle from accelerometer
	float R;                     // Unit vector - total G.

	float gyroRaw[3];            // Raw readings from gyro
	float angle[3];              // Angle from gyro 
	float angleRaw[3];           // Temp for angle-calculation

	float magRaw[3];             // Raw readings from magnetometer
	float magAngle[3];           // Measured angles from magnetometer
	float mx = 0;                // Calculated magnetometer value in x-direction with pan/tilt compensation
	float my = 0;                // Calculated magnetometer value in y-direction with pan/tilt compensation

	int magPosOff[3];
	int magNegOff[3];
	float magGain[3];



	// Final angles for headtracker:
	float tiltAngle = 90;       // Tilt angle
	float tiltAngleLP = 90;     // Tilt angle with low pass filterData
	float lastTiltAngle = 90;   // Used in low pass filterData.

	float rollAngle = 0;        // Roll angle
	float rollAngleLP = 90;     // Roll angle with low pass filterData
	float lastRollAngle = 90;   // Used in low pass filterData

	float panAngle = 90;        // Pan angle
	float panAngleLP = 90;      // Pan angle with low pass filterData
	float lastPanAngle = 90;    // Used in low pass filterData

								// Start values - center position for head tracker
	float tiltStart = 0;
	float panStart = 0;
	float rollStart = 0;

	bool TrackerStarted = false;


	// Settings
	//
public:
	float tiltRollBeta = 0.75;
	float panBeta = 0.75;
	float gyroWeightTiltRoll = 0.98;
	float GyroWeightPan = 0.98;

	int accOffset[3] = { 0, 0, 0 };
	float magOffset[3] = { (MAG0MAX + MAG0MIN) / 2, (MAG1MAX + MAG1MIN) / 2, (MAG2MAX + MAG2MIN) / 2 };
	float gyroOff[3] = { 0, 0, 0 };
	
};

#endif

