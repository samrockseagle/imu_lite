//Developed by KRDP -AKA sam (samrockseagle@gmail.com)
/*
 * IMU Lite is a warpper around 
// Original project by Dennis Frie - 2012 - Dennis.frie@gmail.com
// Discussion: http://www.rcgroups.com/forums/showthread.php?t=1677559
//
// Other contributors to this code:
//  Mark Mansur (Mangus on rcgroups)
 * The original project uses PPM stream and is intended to use with RC radios
 * 
 * This project however uses NRF24L01 and sends digital data packets of length 10 Bytes 
 * Refer to the BUFF_LEN and data_TX with what each channel means
 * 
 * Compiled and tested on Arduino pro mini, Arduino nano so it should run fine 
 * on other ATmega328 based arduinos..
 * 
 * IMU_Lite with NRF24L01 here is used to send data to a car that runs the corresponding receiver.
 */
#include <Wire.h>
#include "Config.h"
#include "Sensors.h"
#include "storage.h"
#include <EEPROM.h>
#include <arduino.h>
#include "timerGlobal.h"
//______
Sensors sens;
Storage stg;
timerGlobal t_main, t_button, t_radio;

//_______________________________________________


#define ESC_CN		90
#define STEER_CN	90
#define PAN_CN		90
#define TILT_CN		90
#define BRAKE_DEF	1
#define LIGHT_DEF	1 // inverted
#define HORN_DEF	0


#define TIMEOUT 20

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

#define BUFF_LEN 10

RF24 radio(8, 7);

const uint64_t pipes[3] = {
	0xF0F0F0F1E1LL, 0xF0F0F0F0D2LL, 0xF0F0F0F0D3LL
};   // Radio pipe addresses for communication.


byte dataTX[BUFF_LEN] = { 'L', ESC_CN, STEER_CN, PAN_CN, TILT_CN, BRAKE_DEF, LIGHT_DEF, HORN_DEF, 0 , 0 }, dataRX[BUFF_LEN];
byte htk[4] = { 0, 0, 0, 91 };
byte ib[12] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

bool htkUpdate = false;
bool lightON = false;
bool valid = false;


void setup()
{
	Serial.begin(SERIAL_BAUD);

	pinMode(BUTTON_INPUT, INPUT);
	digitalWrite(BUTTON_INPUT, HIGH);
	//pinMode(ARDUINO_LED, OUTPUT);    // Arduino LED
	//digitalWrite(ARDUINO_LED, HIGH);
	delay(1000); // Note: only use delay here. This won't work when Timer0 is repurposed later.
	//digitalWrite(ARDUINO_LED, LOW);

	// InitPWMInterrupt();         // Start PWM interrupt  
	Wire.begin();               // Start I2C

								// If the device have just been programmed, write initial config/values to EEPROM:
	if (stg.queryifNew())
	{
		sens.initializeSensors();
#if (ALWAYS_CAL_GYRO)    
		sens.setGyro_offset();
#endif     
		stg.saveSetValues();
		stg.saveMagConf();
		stg.saveAccConf();
	}

	stg.GetData();                 // Get settings saved in EEPROM
	sens.initializeSensors();                // Initialize I2C sensors
	sens.calibMag();
	sens.centerReset();


	//_______________________________________________

	Serial.begin(57600);

	radio.begin();
	radio.setChannel(58);
	radio.setPALevel(RF24_PA_MAX);
	radio.setDataRate(RF24_250KBPS);
	radio.setCRCLength(RF24_CRC_8);
	radio.openWritingPipe(pipes[0]);
	radio.openReadingPipe(1, pipes[1]); //radio.openReadingPipe(1,pipes[1]);

	t_main.timeStart();

}

void loop()
{
	if (digitalRead(BUTTON_INPUT) == LOW)
	{
		if (!t_button.running)
		{
			t_button.timeStart();
		}
		if (t_button.timeReturn() > 50)
		{
			//Serial.println("RESET");
			sens.reset = true;
			t_button.timeStop();
		}
	}
	else
	{
		t_button.timeStop();
	}

	//if (t_main.timeReturn() % 2 == 0) // 
	//{
		sens.sensUpdate();
		sens.gyroCal();
		sens.accelCal();
		sens.magCal();
		sens.filterData();
		sens.tkOut();
	//}
	//if (t_main.timeReturn() % 20 == 0) // 
	//{
	//	Serial.print("HTdata :");
	//	for (byte i = 0; i < 4; ++i)
	//	{
	//		Serial.print("\t :");
	//		Serial.print(sens.htk_data[i]);
	//	}
	//	Serial.println("");
	//}
//}

	carfn();

}


void carfn()
{
	if (!t_radio.running)
	{
		//
		for (byte kk = 0; kk < 12; ++kk)
		{
			ib[kk] = 0;
		}

		if (Serial.available() > 11)
		{
			
			while (Serial.available() > 11) // get stuff from host..
			{
				Serial.readBytesUntil(255, ib, 12);
			}
			Serial.flush();

			if (ib[11] == 255)
			{
				//Serial.println("got something");
				dataTX[1] = map(ib[1], 0, 254, 0, 179); // speed up / down
				dataTX[1] += 5;

				dataTX[2] = map(ib[0], 0, 254, 40, 140); // steer left / right

				if (ib[7] == 70)
				{
					htkUpdate = true;
				}
				else if(ib[7] == 73) {
					htkUpdate = false;
				}

				if (htkUpdate)
				{
					if (sens.htk_data[0] < 180 && sens.htk_data[0] > 0) // tilt  70 to 110
					{
						byte tmp = sens.htk_data[0];
						tmp = map(tmp, 80, 30, 70, 110);
						dataTX[4] = tmp*1.5 - 55;
					}
					if (sens.htk_data[2] < 140 && sens.htk_data[2] > 56) // pan
					{
						dataTX[3] = 1.7 * (180 - sens.htk_data[2]) - 58;
					}
				}

				if (ib[9] == 33) // brake
				{
					dataTX[1] = ESC_CN;
					dataTX[5] = 1;
				}
				else if (ib[10] == 37) //release
				{
					dataTX[5] = 0;
				}

				if (ib[8] == 64)
				{
					dataTX[6] = 0;
				}
				else if (ib[8] == 69)
				{
					dataTX[6] = 1;
				}

				if (ib[6] == 49) // horn
				{
					dataTX[7] = 1;
				}
				else
				{
					dataTX[7] = 0;
				}

				//Serial.println("got data");
				radioSend();
			}
			//printData();
		}
	}

	else
	{
		//Serial.println("radio wait");
		if (!radio.available() && t_radio.timeReturn() < TIMEOUT)
		{		}
		else
		{
			if (t_radio.timeReturn() > TIMEOUT)
			{
				Serial.println("TIME OUT");
			}
			else {
				radio.read(&dataRX, BUFF_LEN);

				if (dataRX[0] == 43)
				{
					printData();
				}
			}
			t_radio.timeStop();
		}
	}

}



void printData()
{
	for (byte k = 0; k < BUFF_LEN - 2; ++k)
	{
		Serial.print(dataRX[k]); Serial.print("\t");
	}
	Serial.print(htkUpdate); Serial.print("\t");
	Serial.println("");
}

void radioSend()
{
	//Serial.println("Sent ");
	radio.stopListening();                          
	radio.write(&dataTX, BUFF_LEN);
	radio.startListening();                         
	t_radio.timeStart();
}
